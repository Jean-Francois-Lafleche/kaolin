��)      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�USD Tutorial�h]�h �Text����USD Tutorial�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�D/home/jlafleche/Projects/kaolin_temp2/doc_src/notes/usd_tutorial.rst�hKubh �	paragraph���)��}�(h��Universal Scene Description (USD) is an open-source 3D scene
description file format developed by Pixar developed to be
versatile, extensible and interchangeable between different 3D
tools.�h]�h��Universal Scene Description (USD) is an open-source 3D scene
description file format developed by Pixar developed to be
versatile, extensible and interchangeable between different 3D
tools.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h��USD supports asset referencing, making it suitable for
organizing entire datasets into interpretable, viewable
subsets based on tags, class or other metadata label.�h]�h��USD supports asset referencing, making it suitable for
organizing entire datasets into interpretable, viewable
subsets based on tags, class or other metadata label.�����}�(hh=hh;hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK	hhhhubh
)��}�(hhh]�(h)��}�(h�(Creating a Mesh Dataset from a USD Scene�h]�h�(Creating a Mesh Dataset from a USD Scene�����}�(hhNhhLhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhIhhhh*hKubh,)��}�(h��Kaolin supports importing meshes from a provided scene file.
Let's use Pixar's `KitchenSet <http://graphics.pixar.com/usd/downloads.html>`_ for our example.�h]�(h�SKaolin supports importing meshes from a provided scene file.
Let’s use Pixar’s �����}�(h�OKaolin supports importing meshes from a provided scene file.
Let's use Pixar's �hhZhhhNhNubh �	reference���)��}�(h�<`KitchenSet <http://graphics.pixar.com/usd/downloads.html>`_�h]�h�
KitchenSet�����}�(h�
KitchenSet�hheubah}�(h]�h!]�h#]�h%]�h']��name��
KitchenSet��refuri��,http://graphics.pixar.com/usd/downloads.html�uh)hchhZubh �target���)��}�(h�/ <http://graphics.pixar.com/usd/downloads.html>�h]�h}�(h]��
kitchenset�ah!]�h#]��
kitchenset�ah%]�h']��refuri�hwuh)hx�
referenced�KhhZubh� for our example.�����}�(h� for our example.�hhZhhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhIhhubh �image���)��}�(h�).. image:: ../_static/img/kitchenset.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��#notes/../_static/img/kitchenset.png��
candidates�}��*�h�suh)h�hhIhhhh*hKubh,)��}�(h�dNext we will create a mesh dataset from this scene. First, we need
to set our environment variables:�h]�h�dNext we will create a mesh dataset from this scene. First, we need
to set our environment variables:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhIhhubh �literal_block���)��}�(h�>>> source setenv.sh�h]�h�>>> source setenv.sh�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��force���language��bash��highlight_args�}�uh)h�hh*hKhhIhhubh,)��}�(h��This step is required for any interactions with USD (this will
be addressed in future releases!). Now, we can create our dataset:�h]�h��This step is required for any interactions with USD (this will
be addressed in future releases!). Now, we can create our dataset:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhIhhubh�)��}�(h��>>> from kaolin.datasets.usdfile import USDMeshes
>>> usd_meshes = USDMeshes(usd_filepath='./data/Kitchen_set/Kitchen_set.usd')
>>> len(usd_meshes)
740�h]�h��>>> from kaolin.datasets.usdfile import USDMeshes
>>> usd_meshes = USDMeshes(usd_filepath='./data/Kitchen_set/Kitchen_set.usd')
>>> len(usd_meshes)
740�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�hĉhŌpython�h�}�uh)h�hh*hK hhIhhubh,)��}�(h�hAnd just like that, we have a dataset of 740 diverse objects for our use!
Let's see what they look like.�h]�h�jAnd just like that, we have a dataset of 740 diverse objects for our use!
Let’s see what they look like.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK'hhIhhubh �doctest_block���)��}�(hX�  >>> from kaolin.visualize.vis_usd import VisUsd
>>> vis = VisUsd()
>>> vis.set_stage()
>>> spacing = 200
>>> for i, sample in enumerate(usd_meshes):
>>>     attr, data = sample['attributes'], sample['data']
>>>     max_x = int(math.sqrt(len(usd_meshes)))
>>>     x = (i % max_x) * spacing
>>>     y = (i // max_x) * spacing
>>>     vis.visualize(mesh, object_path=f'/Root/Visualizer/{attr["name"]}', \
>>>                   translation=(x, y, 0))�h]�hX�  >>> from kaolin.visualize.vis_usd import VisUsd
>>> vis = VisUsd()
>>> vis.set_stage()
>>> spacing = 200
>>> for i, sample in enumerate(usd_meshes):
>>>     attr, data = sample['attributes'], sample['data']
>>>     max_x = int(math.sqrt(len(usd_meshes)))
>>>     x = (i % max_x) * spacing
>>>     y = (i // max_x) * spacing
>>>     vis.visualize(mesh, object_path=f'/Root/Visualizer/{attr["name"]}', \
>>>                   translation=(x, y, 0))�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hhIhhhNhNubh,)��}�(h�OAnd opening the USD in your favourite USD viewer with a bit of styling, we get:�h]�h�OAnd opening the USD in your favourite USD viewer with a bit of styling, we get:�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK7hhIhhubh�)��}�(h�0.. image:: ../_static/img/kitchenset_meshes.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��*notes/../_static/img/kitchenset_meshes.png�h�}�h�j  suh)h�hhIhhhh*hK:ubeh}�(h]��(creating-a-mesh-dataset-from-a-usd-scene�ah!]�h#]��(creating a mesh dataset from a usd scene�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Viewing USD Files�h]�h�Viewing USD Files�����}�(hj-  hj+  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj(  hhhh*hK<ubh,)��}�(hX  USD files can be visualized using Pixar's USDView which you can obtain by visiting
`https://developer.nvidia.com/usd <https://developer.nvidia.com/usd>`_ and selecting the
corresponding platform under *.USD Pre-Built Libraries and Tools*. Note, USDView only supports
python 2.7.�h]�(h�UUSD files can be visualized using Pixar’s USDView which you can obtain by visiting
�����}�(h�SUSD files can be visualized using Pixar's USDView which you can obtain by visiting
�hj9  hhhNhNubhd)��}�(h�F`https://developer.nvidia.com/usd <https://developer.nvidia.com/usd>`_�h]�h� https://developer.nvidia.com/usd�����}�(h� https://developer.nvidia.com/usd�hjB  ubah}�(h]�h!]�h#]�h%]�h']��name�� https://developer.nvidia.com/usd�hv� https://developer.nvidia.com/usd�uh)hchj9  ubhy)��}�(h�# <https://developer.nvidia.com/usd>�h]�h}�(h]��https-developer-nvidia-com-usd�ah!]�h#]�� https://developer.nvidia.com/usd�ah%]�h']��refuri�jS  uh)hxh�Khj9  ubh�0 and selecting the
corresponding platform under �����}�(h�0 and selecting the
corresponding platform under �hj9  hhhNhNubh �emphasis���)��}�(h�$*.USD Pre-Built Libraries and Tools*�h]�h�".USD Pre-Built Libraries and Tools�����}�(h�".USD Pre-Built Libraries and Tools�hjh  ubah}�(h]�h!]�h#]�h%]�h']�uh)jf  hj9  ubh�). Note, USDView only supports
python 2.7.�����}�(h�). Note, USDView only supports
python 2.7.�hj9  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK=hj(  hhubeh}�(h]��viewing-usd-files�ah!]�h#]��viewing usd files�ah%]�h']�uh)h	hhhhhh*hK<ubh
)��}�(hhh]�(h)��}�(h�
Some Notes�h]�h�
Some Notes�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKCubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�<Currently, \*.usd and \*.usda file extensions are supported.�h]�h,)��}�(hj�  h]�h�:Currently, *.usd and *.usda file extensions are supported.�����}�(h�<Currently, \*.usd and \*.usda file extensions are supported.�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  hhhh*hNubj�  )��}�(h�bWhen importing meshes, any mesh that has a varying number of vertices per face cannot be imported.�h]�h,)��}�(hj�  h]�h�bWhen importing meshes, any mesh that has a varying number of vertices per face cannot be imported.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKFhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)j�  hh*hKEhj�  hhubeh}�(h]��
some-notes�ah!]�h#]��
some notes�ah%]�h']�uh)h	hhhhhh*hKCubeh}�(h]��usd-tutorial�ah!]�h#]��usd tutorial�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j%  j"  h�h�j�  j�  j]  jZ  j�  j�  u�	nametypes�}�(j�  Nj%  Nh��j�  Nj]  �j�  Nuh}�(j�  hj"  hIh�hzj�  j(  jZ  jT  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.