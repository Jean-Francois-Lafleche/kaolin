���<      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Datasets�h]�h �Text����Datasets�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�I/home/jlafleche/Projects/kaolin_temp2/doc_src/notes/datasets_tutorial.rst�hKubh �	paragraph���)��}�(h��One of the very first things you would want to do, for a new 3D deep learning
application, is to load data into a format that PyTorch can operate upon.
With Kaolin, the process is a breeze, as we have done most of the hard work
for you!�h]�h��One of the very first things you would want to do, for a new 3D deep learning
application, is to load data into a format that PyTorch can operate upon.
With Kaolin, the process is a breeze, as we have done most of the hard work
for you!�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �topic���)��}�(hhh]�h �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(hhh]�h,)��}�(hhh]�h �	reference���)��}�(hhh]�h�ShapeNet�����}�(h�ShapeNet�hhOubah}�(h]��id2�ah!]�h#]�h%]�h']��refid��shapenet�uh)hMhhJubah}�(h]�h!]�h#]�h%]�h']�uh)h+hhGubah}�(h]�h!]�h#]�h%]�h']�uh)hEhhBubhF)��}�(hhh]�h,)��}�(hhh]�hN)��}�(hhh]�h�ModelNet�����}�(h�ModelNet�hhrubah}�(h]��id3�ah!]�h#]�h%]�h']��refid��modelnet�uh)hMhhoubah}�(h]�h!]�h#]�h%]�h']�uh)h+hhlubah}�(h]�h!]�h#]�h%]�h']�uh)hEhhBubhF)��}�(hhh]�h,)��}�(hhh]�hN)��}�(hhh]�h�SHREC16�����}�(h�SHREC16�hh�ubah}�(h]��id4�ah!]�h#]�h%]�h']��refid��shrec16�uh)hMhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hEhhBubhF)��}�(hhh]�h,)��}�(hhh]�hN)��}�(hhh]�h�More to come�����}�(h�More to come�hh�ubah}�(h]��id5�ah!]�h#]�h%]�h']��refid��more-to-come�uh)hMhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)hEhhBubeh}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhNhNubah}�(h]��contents�ah!]�(�contents��local�eh#]��contents�ah%]�h']�uh)h;hh*hK
hhhhubh
)��}�(hhh]�(h)��}�(hhVh]�h�ShapeNet�����}�(hhVhh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']��refid�hYuh)hhh�hhhh*hKubh �image���)��}�(h�%.. image:: /_static/img/ShapeNet.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��_static/img/ShapeNet.png��
candidates�}��*�j  suh)h�hh�hhhh*hKubh,)��}�(hXb  `ShapeNet <https://shapenet.org/>`_ is an extremely popular (and huge!) repository
of objects encoded as triangle meshes, and is often used in 3D deep learning.
However, you will first need to obtain access to the dataset (agree to terms of use,
download, unzip), and then you can leverage the power of Kaolin to load up ShapeNet
data in several formats.�h]�(hN)��}�(h�#`ShapeNet <https://shapenet.org/>`_�h]�h�ShapeNet�����}�(h�ShapeNet�hj  ubah}�(h]�h!]�h#]�h%]�h']��name��ShapeNet��refuri��https://shapenet.org/�uh)hMhj  ubh �target���)��}�(h� <https://shapenet.org/>�h]�h}�(h]��id1�ah!]�h#]��shapenet�ah%]�h']��refuri�j  uh)j  �
referenced�Khj  ubhX?   is an extremely popular (and huge!) repository
of objects encoded as triangle meshes, and is often used in 3D deep learning.
However, you will first need to obtain access to the dataset (agree to terms of use,
download, unzip), and then you can leverage the power of Kaolin to load up ShapeNet
data in several formats.�����}�(hX?   is an extremely popular (and huge!) repository
of objects encoded as triangle meshes, and is often used in 3D deep learning.
However, you will first need to obtain access to the dataset (agree to terms of use,
download, unzip), and then you can leverage the power of Kaolin to load up ShapeNet
data in several formats.�hj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh,)��}�(hX�  Kaolin supports multiple formats in which you can load a ShapeNet class (eg. meshes,
pointclouds, voxels, signed distance functions, and more). Although ShapeNet objects
are usually available only as meshes, Kaolin internally converts to other
representations and returns them. This means that, the first time you load a
ShapeNet category in a non-native (i.e., non mesh) format, it will probably take a
while, as it is internally converting, and then caching data.�h]�hX�  Kaolin supports multiple formats in which you can load a ShapeNet class (eg. meshes,
pointclouds, voxels, signed distance functions, and more). Although ShapeNet objects
are usually available only as meshes, Kaolin internally converts to other
representations and returns them. This means that, the first time you load a
ShapeNet category in a non-native (i.e., non mesh) format, it will probably take a
while, as it is internally converting, and then caching data.�����}�(hj;  hj9  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh,)��}�(h��Assuming that the ShapeNet directory is located at a path `shapenet_dir`, here's
how to load meshes from the ShapeNet `chair` category.�h]�(h�:Assuming that the ShapeNet directory is located at a path �����}�(h�:Assuming that the ShapeNet directory is located at a path �hjG  hhhNhNubh �title_reference���)��}�(h�`shapenet_dir`�h]�h�shapenet_dir�����}�(h�shapenet_dir�hjR  ubah}�(h]�h!]�h#]�h%]�h']�uh)jP  hjG  ubh�0, here’s
how to load meshes from the ShapeNet �����}�(h�., here's
how to load meshes from the ShapeNet �hjG  hhhNhNubjQ  )��}�(h�`chair`�h]�h�chair�����}�(h�chair�hjf  ubah}�(h]�h!]�h#]�h%]�h']�uh)jP  hjG  ubh�
 category.�����}�(h�
 category.�hjG  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhh�hhubh �literal_block���)��}�(h��>>> from kaolin.datasets import shapenet
>>> from torch.utils.data import DataLoader
>>> meshes = shapenet.ShapeNet_Meshes(root=shapenet_dir, categories=['plane'])�h]�h��>>> from kaolin.datasets import shapenet
>>> from torch.utils.data import DataLoader
>>> meshes = shapenet.ShapeNet_Meshes(root=shapenet_dir, categories=['plane'])�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��force���language��python��highlight_args�}�uh)j�  hh*hK!hh�hhubh�)��}�(h�).. image:: /_static/img/planes_mesh.png

�h]�h}�(h]�h!]�h#]�h%]�h']��uri��_static/img/planes_mesh.png�j  }�j  j�  suh)h�hh�hhhh*hK)ubh,)��}�(h�3Loading multiple categories is straightforward too.�h]�h�3Loading multiple categories is straightforward too.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK*hh�hhubj�  )��}�(h�V>>> meshes = shapenet.ShapeNet_Meshes(root=shapenet_dir,
categories=['chair', 'bowl'])�h]�h�V>>> meshes = shapenet.ShapeNet_Meshes(root=shapenet_dir,
categories=['chair', 'bowl'])�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  j�  �j�  �python�j�  }�uh)j�  hh*hK,hh�hhubh,)��}�(h��You can either specify the `categories` by their plaintext names
(eg. ``chair``, ``bowl``, etc.) or use their ``synset_id``
(eg. ``03636649``, ``02924116``, etc.)�h]�(h�You can either specify the �����}�(h�You can either specify the �hj�  hhhNhNubjQ  )��}�(h�`categories`�h]�h�
categories�����}�(h�
categories�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)jP  hj�  ubh� by their plaintext names
(eg. �����}�(h� by their plaintext names
(eg. �hj�  hhhNhNubh �literal���)��}�(h�	``chair``�h]�h�chair�����}�(h�chair�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�, �����}�(h�, �hj�  hhhNhNubj�  )��}�(h�``bowl``�h]�h�bowl�����}�(h�bowl�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�, etc.) or use their �����}�(h�, etc.) or use their �hj�  hhhNhNubj�  )��}�(h�``synset_id``�h]�h�	synset_id�����}�(h�	synset_id�hj	  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�
(eg. �����}�(h�
(eg. �hj�  hhhNhNubj�  )��}�(h�``03636649``�h]�h�03636649�����}�(h�03636649�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�, �����}�(hj�  hj�  ubj�  )��}�(h�``02924116``�h]�h�02924116�����}�(h�02924116�hj0  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubh�, etc.)�����}�(h�, etc.)�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK1hh�hhubh,)��}�(h��Suppose you need a different representation than meshes. You can easily load different
representations, such as voxels and point clouds.�h]�h��Suppose you need a different representation than meshes. You can easily load different
representations, such as voxels and point clouds.�����}�(hjL  hjJ  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK5hh�hhubj�  )��}�(h��>>> voxels = shapenet.ShapeNet_Voxels(root=shapenet_dir, categories=['plane'])
>>> points = shapenet.ShapeNet_Points(root=shapenet_dir, categories=['plane'])�h]�h��>>> voxels = shapenet.ShapeNet_Voxels(root=shapenet_dir, categories=['plane'])
>>> points = shapenet.ShapeNet_Points(root=shapenet_dir, categories=['plane'])�����}�(hhhjX  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  j�  �j�  �python�j�  }�uh)j�  hh*hK8hh�hhubh,)��}�(h�cThis can now be used to initialize a Pytorch dataloader, in a similar way as
you would for 2D data.�h]�h�cThis can now be used to initialize a Pytorch dataloader, in a similar way as
you would for 2D data.�����}�(hjj  hjh  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK=hh�hhubj�  )��}�(h��>>> voxel_dataloader = DataLoader(voxels, batch_size=10, shuffle=True, num_workers=8)
>>> point_dataloader = DataLoader(points, batch_size=10, shuffle=True, num_workers=8)�h]�h��>>> voxel_dataloader = DataLoader(voxels, batch_size=10, shuffle=True, num_workers=8)
>>> point_dataloader = DataLoader(points, batch_size=10, shuffle=True, num_workers=8)�����}�(hhhjv  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  j�  �j�  �python�j�  }�uh)j�  hh*hK@hh�hhubh�)��}�(h�:.. image:: /_static/img/planes_voxels.png
    :width: 49 %�h]�h}�(h]�h!]�h#]�h%]�h']��width��49%��uri��_static/img/planes_voxels.png�j  }�j  j�  suh)h�hh�hhhh*hNubh�)��}�(h�8.. image:: /_static/img/planes_pc.png
    :width: 49 %

�h]�h}�(h]�h!]�h#]�h%]�h']��width��49%��uri��_static/img/planes_pc.png�j  }�j  j�  suh)h�hh�hhhh*hNubeh}�(h]�h_ah!]�h#]�h%]�j)  ah']�uh)h	hhhhhh*hKj-  Kubh
)��}�(hhh]�(h)��}�(hhyh]�h�ModelNet�����}�(hhyhj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�h�h|uh)hhj�  hhhh*hKMubh,)��}�(h�/ModelNet voxels can be loaded in a similar way:�h]�h�/ModelNet voxels can be loaded in a similar way:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKOhj�  hhubj�  )��}�(h��>>> from kaolin.datasets import modelnet
>>> voxels = modelnet.ModelNet(root=shapenet_dir, categories=['plane'])
>>> dataloader = DataLoader(voxels, batch_size=10, shuffle=True, num_workers=8)�h]�h��>>> from kaolin.datasets import modelnet
>>> voxels = modelnet.ModelNet(root=shapenet_dir, categories=['plane'])
>>> dataloader = DataLoader(voxels, batch_size=10, shuffle=True, num_workers=8)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  j�  �j�  �python�j�  }�uh)j�  hh*hKQhj�  hhubeh}�(h]�h�ah!]�h#]��modelnet�ah%]�h']�uh)h	hhhhhh*hKMubh
)��}�(hhh]�(h)��}�(hh�h]�h�SHREC16�����}�(hh�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)hhj�  hhhh*hKYubh�)��}�(h�".. image:: /_static/img/shrec.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��_static/img/shrec.png�j  }�j  j�  suh)h�hj�  hhhh*hK[ubh,)��}�(h��SHREC is a dataset that was curated for the
`Large-Scale 3D Retrieval From ShapeNet Core55` challenge at Eurographics 2016.
We implement a mesh dataloader for SHREC16.�h]�(h�,SHREC is a dataset that was curated for the
�����}�(h�,SHREC is a dataset that was curated for the
�hj�  hhhNhNubjQ  )��}�(h�/`Large-Scale 3D Retrieval From ShapeNet Core55`�h]�h�-Large-Scale 3D Retrieval From ShapeNet Core55�����}�(h�-Large-Scale 3D Retrieval From ShapeNet Core55�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)jP  hj�  ubh�L challenge at Eurographics 2016.
We implement a mesh dataloader for SHREC16.�����}�(h�L challenge at Eurographics 2016.
We implement a mesh dataloader for SHREC16.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK\hj�  hhubj�  )��}�(h��>>> from kaolin.datasets import shrec
>>> meshes = shrec.SHREC16(root=shapenet_dir, categories=['plane'])
>>> dataloader = DataLoader(meshes, batch_size=10, shuffle=True, num_workers=8)�h]�h��>>> from kaolin.datasets import shrec
>>> meshes = shrec.SHREC16(root=shapenet_dir, categories=['plane'])
>>> dataloader = DataLoader(meshes, batch_size=10, shuffle=True, num_workers=8)�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  j�  �j�  �python�j�  }�uh)j�  hh*hK`hj�  hhubeh}�(h]�h�ah!]�h#]��shrec16�ah%]�h']�uh)h	hhhhhh*hKYubh
)��}�(hhh]�(h)��}�(hh�h]�h�More to come�����}�(hh�hj9  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)hhj6  hhhh*hKhubh,)��}�(h�[Kaolin supports a bunch of other datasets too. Stay tuned to this space for more tutorials.�h]�h�[Kaolin supports a bunch of other datasets too. Stay tuned to this space for more tutorials.�����}�(hjH  hjF  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKihj6  hhubeh}�(h]�h�ah!]�h#]��more to come�ah%]�h']�uh)h	hhhhhh*hKhubeh}�(h]��datasets�ah!]�h#]��datasets�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j`  j]  h�h݌shapenet�j&  j�  h�j3  h�jX  h�u�	nametypes�}�(j`  Nh�Nj�  �j�  Nj3  NjX  Nuh}�(j]  hh�h=h_h�j&  j   h�j�  h�j�  h�j6  hYhOh|hrh�h�h�h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h,)��}�(h�+Duplicate implicit target name: "shapenet".�h]�h�/Duplicate implicit target name: “shapenet”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']�j&  a�level�K�type��INFO��source�h*�line�Kuh)j�  hh�hhhh*hKuba�transform_messages�]��transformer�N�
decoration�Nhhub.